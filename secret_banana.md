# Secret Banana

Created by Mike Boxleiter, Tommy Maranges, and Mac Schubert. Used with permission under a Creative Commons Attribution-NonCommercial-Sharealike 4.0 International License

A re"skin" of that other game you might find more a-peeling. 

## Playing Cards Edition

|Function|Cards|
|---|---|
|Fruits|K♠️, Q♠️, J♠️|
|Banana|Joker|
|Vegetables|K♥️, Q♥️, J♥️, K♦️, Q♦️, J♦️|
|Voting Cards YES|A♥️ - 10♥️|
|Voting Cards NO|A♠️ - 10♠️|
|Fruit Policies|A♣️ - J♣️|
|Vegetable Policies|A♦️ - 6♦️|
|President|K♣️|
|Deputy|Q♣️|

## Overview

The Vegetables are trying to hold a stable government together and advance freedom, but the Fruits oppose them. One of them is Banana. Eat the Banana (thus, disarming him), and things may be ok. At the very least, don't let them be elected Deputy, or all will be lost. 

The game is played with Vegetables having a majority but not knowing who the enemy is. The fruits are a minority but know each player's identity. In larger games Banana doesn't know his teammates. 

Each round, the President will nominate someone to be Deputy. The players vote, and if a government is successfully formed, the President and Deputy work together to enact a policy, which is either Vegetable (Good) or Fruit (Bad). If a Fruit policy is enacted, the President sometimes gains executive power to wield. Vegetables using these powers wisely can gain information they need to defeat the Fruits. 

## How to Win

The Vegetables win by killing Banana, or by enacting five Vegetable policies. 

The Fruits win by getting Banana elected Deputy after three Fruit policies have been enacted, or by enacting six Fruit policies. 

## Set Up

Divide up a deck of cards by suit and give each player a pair of voting cards from the Heart and Club suit pip cards (A♥️ through 10♥️, and A♠️ through 10♠️).  The heart is a "YES" vote and the club is a "NO" vote. 

Shuffle together the A♦️ through the 6♦️, and the A♣️ through J♣️ to form the policy deck. 

Set the K♣️ and Q♣️ in the middle of the table. 

Choose the appropriate number of Vegetables (Red Face Cards), Fruits (Spade Face Cards) and one Banana: 

|Players|Vegetables|Fruits|Banana|
|---|---|---|---|
|10|6|3|1|
|9|5|3|1|
|8|5|2|1|
|7|4|2|1|
|6|4|1|1|
|5|3|1|1|

Deal one role card to each player. Instruct them to look at it in secret and memorize their role. Once all players understand their secret role, read this script. 

## Revealing Banana and the Fruits

5-6 Players (Banana knows the other Fruits): "Everyone, close your eyes. Banana and Fruits, open your eyes and acknowledge each other. (Long Pause) Everyone, close your eyes. Everyone, open your eyes. If anyone is confused or something went wrong, please tell the group now."

7-10 Players (Banana doesn't know other Fruits): "Everyone, close your eyes and place your hand in a fist on the table. Banana, keep your eyes closed but please put your hand in a "Thumbs Up" position where everyone could see. Fruits, please open your eyes and acknowledge each other and take note of who is Banana. (Long Pause) Everyone please close your eyes, Banana, return your hand to normal. Everyone please remove your hands from the table and open your eyes. If anyone is confused or something went wrong, please tell the group now." 

## Game Play

Players may lie about any information in the game except where the game rules say the player must answer truthfully. This normally happens when a player is Eaten or is elected Deputy late in the game. 

### Election

1. The first President (K♣️) candidate is chosen randomly. The President nominates a Deputy (Q♣️) candidate. 

1.a. Term Limit - The President candidate may not choose a Deputy candidate that was part of the last government (as President or Deputy) that enacted a policy. If there are only 5 or fewer players alive, only the last Deputy is ineligible.

2. The players all vote on the proposed President/Deputy slate by choosing YES or NO vote cards in secret and revealing them at the same time. A majority is required. Everyone votes. Ties fail. 

3. Failed Government - If the vote is no, a "Failed Government" results. On the third failed government in a row, the top card of the policy deck is turned over and enacted. This policy enactment does not trigger an executive power. Pass the presidency to the next player. Reset the failed government tracker. Term Limits expire. 

4. If the vote is yes, the President and Deputy candidates are installed as the Government. There is a legislative session and potential executive action. 

4.a. If there have been three Fruit policies already enacted, ask the new Deputy whether they are Banana. The Deputy must answer this question truthfully. If three Fruity policies have been enacted and Banana is elected, Banana and the Fruits win the game.

5. After legislation and executive action, the next player clockwise becomes the presidential candidate.

### Legislation

1. The President takes three cards from the policy deck, discards one in secret, then gives the remaining two cards to the Deputy. The Deputy discards one in secret, then the remaining policy is enacted. 

2. The President and Deputy must not exchange any verbal or non-verbal communication until a policy has been enacted.

3. If there are fewer than three cards left in the policy deck after enacting a policy, shuffle the deck with the discard pile to form the new policy deck. The unused policy cards should not be revealed. 

4. Enacting a Fruit policy sometimes grants the sitting President an executive power. 

5. Enacting a Vegetable policy does nothing. 

### Executive Action

If a Fruit policy is enacted, check the Fruity Powers table below. The current President must use the power for the game to continue. Players may discuss how the power is to be used, but the current President makes the final decision. Using the power is not optional.

#### Fruity Powers

|Players|1|2|3|4|5|6|
|---|---|---|---|---|---|---|
|5-6|Nothing|Nothing|Policy Peek|Eat a Player|Eat a Player|Fruits Win|
|7-8|Nothing|Investigate|Call Special Election|Eat a Player|Eat a Player|Fruits Win|
|9-10|Investigate|Investigate|Call Special Election|Eat a Player|Eat a Player|Fruits Win|

#### Fruity Powers Described

Investigate: The President asks another player "Are you a Vegetable?" and the player will use their voting cards to truthfully answer YES or NO to the question in secret. Only the President may see the voting card. The president may share or lie about this information. 

Policy Peek: The President looks at the next three cards of the policy deck and returns them in the same order.

Call Special Election: The President chooses another player to be the next presidential candidate. Any player can be chosen, even if they were term-limited. After the special election the presidency passes to the person that would have been in line before the special election. This can cause the same person to be President candidate twice in a row. 

Execution: The president chooses one player at the table and says "I eat [Player Name]." The player must answer truthfully whether they are Banana. If the player is Banana, the Vegetables win the game. The table does not learn the identity of the player (Fruit or Vegetable), only whether the killed player is Banana. Executed players do not speak, vote, or run for office. 

### Veto Power

After five fruit policies have been enacted, the President and Deputy gain veto power. When the Deputy is looking at two potential policies to be enacted, the Deputy may say "I propose to veto this slate". If the President agrees, the policies are discarded and the presidency passes to the next player. This causes a Failed Government (advances election tracker by one). If the President disagrees, the Deputy must choose a policy to enact. 
