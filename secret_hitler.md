# Secret Hitler

Created by Mike Boxleiter, Tommy Maranges, and Mac Schubert. Used with permission under a Creative Commons Attribution-NonCommercial-Sharealike 4.0 International License

## Playing Cards Edition

|Function|Cards|
|---|---|
|Fascists|K♠️, Q♠️, J♠️|
|Hitler|Joker|
|Liberals|K♥️, Q♥️, J♥️, K♦️, Q♦️, J♦️|
|Voting Cards YES|A♥️ - 10♥️|
|Voting Cards NO|A♠️ - 10♠️|
|Fascist Policies|A♣️ - J♣️|
|Liberal Policies|A♦️ - 6♦️|
|President|K♣️|
|Chancellor|Q♣️|

## Overview

The Liberals are trying to hold a stable government together and advance freedom, but the fascists oppose them. One of them is Hitler. Kill Hitler, and things may be ok. At the very least, don't let him be elected chancellor, or all will be lost. 

The game is played with liberals having a majority but not knowing who the enemy is. The fascists are a minority but know each player's identity. In larger games Hitler doesn't know his teammates. 

Each round, the president will nominate someone to be chancellor. The players vote, and if a government is successfully formed, the president and chancellor work together to enact a policy, which is either fascist or liberal. If a fascist policy is enacted, the president sometimes gains executive power to wield. Liberals using these powers wisely can gain information they need to defeat the fascists. 

## How to Win

The liberals win by killing Hitler, or by enacting five liberal policies. 

The fascists win by getting Hitler elected Chancellor after three fascist policies have been enacted, or by enacting six fascist policies. 

## Set Up

Divide up a deck of cards by suit and give each player a pair of voting cards from the Heart and Club suit pip cards (A♥️ through 10♥️, and A♠️ through 10♠️).  The heart is a "YES" vote and the club is a "NO" vote. 

Shuffle together the A♦️ through the 6♦️, and the A♣️ through J♣️ to form the policy deck. 

Set the K♣️ and Q♣️ in the middle of the table. 

Choose the appropriate number of Liberals (Red Face Cards), Fascists (Spade Face Cards) and Hitler: 

|Players|Liberals|Fascists|Hitler|
|---|---|---|---|
|10|6|3|1|
|9|5|3|1|
|8|5|2|1|
|7|4|2|1|
|6|4|1|1|
|5|3|1|1|

Deal one role card to each player. Instruct them to look at it in secret and memorize their role. Once all players understand their secret role, read this script. 

## Revealing Hitler and the Fascists

5-6 Players (Hitler knows the other Fascist): "Everyone, close your eyes. Hitler and Fascist, open your eyes and acknowledge each other. (Long Pause) Everyone, close your eyes. Everyone, open your eyes. If anyone is confused or something went wrong, please tell the group now."

7-10 Players (Hitler doesn't know other Fascist): "Everyone, close your eyes and place your hand in a fist on the table. Hitler keep your eyes closed but please put your hand in a "Thumbs Up" position where everyone could see. Fascists, please open your eyes and acknowledge each other and take note of who is Hitler. (Long Pause) Everyone please close your eyes, Hitler, return your hand to normal. Everyone please remove your hands from the table and open your eyes. If anyone is confused or something went wrong, please tell the group now." 

## Game Play

Players may lie about any information in the game except where the game rules say the player must answer truthfully. This normally happens when a player is executed or is elected Chancellor late in the game. 

### Election

1. The first president (K♣️) candidate is chosen randomly. The president nominates a chancellor (Q♣️) candidate. 

1.a. Term Limit - The President candidate may not choose a Chancellor candidate that was part of the last government (as President or Chancellor) that enacted a policy. If there are only 5 or fewer players alive, only the last Chancellor is ineligible.

2. The players all vote on the proposed President/Chancellor slate by choosing YES or NO vote cards in secret and revealing them at the same time. A majority is required. Everyone votes. Ties fail. 

3. Failed Government - If the vote is no, a "Failed Government" results. On the third failed government in a row, the top card of the policy deck is turned over and enacted. This policy enactment does not trigger an executive power. Pass the presidency to the next player. Reset the failed government tracker. Term Limits expire. 

4. If the vote is yes, the President and Chancellor candidates are installed as the Government. There is a legislative session and potential executive action. 

4.a. If there have been three Fascist policies already enacted, ask the new Chancellor whether they are Hitler. The Chancellor must answer this question truthfully. If three fascist policies have been enacted and Hitler is elected, Hitler and the Fascists win the game.

5. After legislation and executive action, the next player clockwise becomes the presidential candidate.

### Legislation

1. The president takes three cards from the policy deck, discards one in secret, then gives the remaining two cards to the Chancellor. The Chancellor discards one in secret, then the remaining policy is enacted. 

2. The president and chancellor must not exchange any verbal or non-verbal communication until a policy has been enacted.

3. If there are fewer than three cards left in the policy deck after enacting a policy, shuffle the deck with the discard pile to form the new policy deck. The unused policy cards should not be revealed. 

4. Enacting a fascist policy sometimes grants the sitting President an executive power. 

5. Enacting a liberal policy does nothing. 

### Executive Action

If a fascist policy is enacted, check the Fascist Powers table below. The current president must use the power for the game to continue. Players may discuss how the power is to be used, but the current president makes the final decision. Using the power is not optional.

#### Fascist Powers

|Players|1|2|3|4|5|6|
|---|---|---|---|---|---|---|
|5-6|Nothing|Nothing|Policy Peek|Execution|Execution|Fascist Win|
|7-8|Nothing|Investigate|Call Special Election|Execution|Execution|Fascist Win|
|9-10|Investigate|Investigate|Call Special Election|Execution|Execution|Fascist Win|

#### Fascist Powers Described

Investigate: The President asks another player "Are you a liberal?" and the player will use their voting cards to truthfully answer YES or NO to the question in secret. Only the President may see the voting card. The president may share or lie about this information. 

Policy Peek: The president looks at the next three cards of the policy deck and returns them in the same order.

Call Special Election: The president chooses another player to be the next presidential candidate. Any player can be chosen, even term-limited. After the special election the presidency passes to the person that would have been in line before the special election. This can cause the same person to be President candidate twice in a row. 

Execution: The president chooses one player at the table and says "I formally execute [Player Name]." The player must answer truthfully whether they are Hitler. If the player is Hitler, the Liberals win the game. The table does not learn the identity of the player (Fascist or Liberal), only whether the killed player is Hitler. Executed players do not speak, vote, or run for office. 

### Veto Power

After five fascists policies have been enacted, the President and Chancellor gain veto power. When the Chancellor is looking at two potential policies to be enacted, the Chancellor may say "I propose to veto this slate". If the President agrees, the policies are discarded and the presidency passes to the next player. This causes a Failed Government (advances election tracker by one). If the President disagrees, the Chancellor must choose a policy to enact. 